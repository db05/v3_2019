import React from 'react';
import AddProduct from './AddProduct.js';

export class ProductList extends React.Component {
    constructor(){
        super();
        this.state = {
            products: []
        };
        this.addProduct = (product) => {
			this.setState( prevState => ({
                products: [...prevState.products, product]
            }));
		}
    }

    render(){
        return(
            <div>
				<AddProduct onAdd={this.addProduct}/>
            </div>
        )
    }
}