import React from 'react';

export class AddProduct extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            category: '',
            price: ''
        };
    }

    addProduct = () => {
        let product = {
            name: this.state.name,
            category: this.state.category,
            price: this.state.price
        };
        this.props.onAdd(product);
    }
    
     handleChangeName = (el) => {
        this.setState({
            name: el.target.value
        })
    }

    handleChangeCategory = (el) => {
        this.setState({
            category: el.target.value
        })
    }

    handleChangePrice = (el) => {
        this.setState({
            price: el.target.value
        })
    }

    render(){
        return (
            <div>
            <input  id="name" onChange={this.handleChangeName} name="name" />
            <input  id="category" onChange={this.handleChangeCategory} name="category"/>
              <input id="price" onChange={this.handleChangePrice} name="price"/>
             <button value="add product" onClick={this.addProduct}>Add product</button>
            </div>
        )
    }
}
export default AddProduct;